# Planet configuration file

# The following rules apply for a new language planet:
#
# * The feeds contain the same content as one would put on planet.debian.org,
#   ie. mostly Debian related / from people involved in Debian
#
# * The feeds provide an own category with only the language for this planet.
#
# * At least 10 feeds have to be there before a new language gets added.
#
# * Language planets will appear as planet.debian.org/$LANGUAGE,
#   where $LANGUAGE will be the two-letter code for it.


# Little documentation for multi-language planet:

# In the following, above "Do not change", replace $LANGUAGE with the
# name of your language, $2LETTERCODE with the 2 letter iso code.
# For example, a german planet would use Deutsch and DE.
# Feel free to edit the other values as shown.
# Please do not touch the config values below "Do not change", just
# skip to the feed list.

# When you are done, send this file to planet@debian.org and ask for the
# addition of the new language planet. Do NOT just commit it, it won't get
# picked up by the scripts.

# After the new language is activated, feel free to edit this file following
# the normal rules for all planet config.ini files.

# Hint: You can use hackergotchis the same way as on main planet. But it is
# *one* central storage for hackergotchis, not one per different language.


# Every planet needs a [Planet] section
[Planet]
# name: Your planet's name.
name = Planet Debian Spanish
# link: Link to the main planet page
link = https://planet.debian.org/es/
# language: short language code for the <html ... lang=""> tag
language = es

# output_dir: Directory to place output files. Add your 2letter Language code
output_dir = www/es
# date_format: strftime format for the default 'date' template variable
date_format = %d %B, %Y %I:%M%p
# new_date_format: strftime format for the 'new_date' template variable
new_date_format = %B %d, %Y
# cache_directory: Where cached feeds are stored. Add your 2letter Language code
cache_directory = cache/es

# Translate the following to your language. Do not change the names, just the
# text after the =
syndicationtext = Se dispone de un feed completo en cualquiera de los formatos de sindicación, obtén el enlace correspondiente de uno de los siguientes botones.
searchtext = Búsqueda
lastupdatetext = Última actualización:
utctext = Todos los tiempos están en UTC.
contacttext = Contacto:
hiddenfeedstext = Feeds ocultos
hiddenfeedstext2 = Tienes entradas ocultas.
showalltext = Mostrar todo
subscriptionstext = Subscripciones
feedtext = feed
otherplanettext = Planetarium

# Do not change config values below here, just skip to the feeds
# Do not change config values below here, just skip to the feeds
owner_name = Debian Planet Maintainers
owner_email = planet@debian.org

# Currently no search for Language planets
search = false
# new_feed_items: Number of items to take from new feeds
# log_level: One of DEBUG, INFO, WARNING, ERROR or CRITICAL
new_feed_items = 5
log_level = DEBUG
spider_threads = 15

# template_files: Space-separated list of output template files
template_files = git/debian/templates/index.html.dj git/debian/templates/atom.xml.dj git/debian/templates/rss20.xml.dj git/debian/templates/rss10.xml.dj git/debian/templates/opml.xml.dj git/debian/templates/foafroll.xml.dj

# The following provide defaults for each template:
# items_per_page: How many items to put on each page
# days_per_page: How many complete days of posts to put on each page
#                This is the absolute, hard limit (over the item limit)
# encoding: output encoding for the file, Python 2.3+ users can use the
#           special "xml" value to output ASCII with XML character references
# locale: locale to use for (e.g.) strings in dates, default is taken from your
#         system
items_per_page = 60
days_per_page = 0
encoding = utf-8
# locale = C

filters = remove-trackers-and-ads.plugin
filter_dir = code/filters

[git/debian/templates/index.html.dj]
date_format = %I:%M%P


# Options placed in the [DEFAULT] section provide defaults for the feed
# sections.  Placing a default here means you only need to override the
# special cases later.
[DEFAULT]
# Hackergotchi default size.
# If we want to put a face alongside a feed, and it's this size, we
# can omit these variables.
facewidth = 65
faceheight = 85
future_dates = ignore_date


############################## FEEDS ##############################
#
# ADD YOURSELF IN ALPHABETICAL ORDER BELOW
#
###################################################################

# The URL of the feed goes in the []s.
# name = Your name
# face = filename of your hackergotchi in heads (or leave out entirely)
# facewidth/faceheight = size of your hackergotchi, if not default

[http://blog.inittab.org/feed/]
name = Alberto Gonzalez Iniesta

[http://alerios.blogspot.com/atom.xml]
name = Alejandro Ríos P.

[http://ekaia.org/blog/feed/planet-debian-es.rss]
name = Ana Beatriz Guerrero Lopez
face = ana.png
facewidth = 90
faceheight = 88

[http://feeds.feedburner.com/k-rolus]
name = Carlos Galisteo

[http://blog.fcestrada.com/category/spanish/planetadebian/feed/]
name = Fernando C Estrada

# 2011-12-14 - joerg - NXDOMAIN
# [http://blog.bureado.com.ve/?feed=rss2&cat=13]
# name = José Parrella
# face = bureado.png
# facewidth = 80
# faceheight = 98

# 2017-12-01 - ana - hacked again
# [http://www.itais.net/feed/atom/]
# name = José Luis Redrejo

[http://ghostbar.co/feed-planetadebian.xml]
name = José Luis Rivas

[http://larjona.wordpress.com/tag/espanol/feed/]
name = Laura Arjona

[http://www.perezmeyer.blogspot.com/feeds/posts/default/-/planetdebian-es?alt=rss]
name = Lisandro Damián Nicanor Pérez Meyer
face = lisandropm.png
facewidth = 78
faceheight = 100

[http://www.lucianobello.com.ar/category/geek/debian/planeta/feed/]
name = Luciano Bello
face = lucianobello.png
facewidth = 65
faceheight = 70

[http://luisuribe.wordpress.com/category/debian/feed/]
name = Luis Uribe
face = luisuribe.png
facewidth = 65
faceheight = 84

[http://soleup.eup.uva.es/mario/rss/atom/1]
name = Mario Izquierdo

# 2011-12-14 - joerg - NXDOMAIN
# [http://blogs.lug.fi.uba.ar/marga/?tempskin=_rss2&cat=31]
# name = Margarita Manterola
# face = marga.png
# facewidth = 65
# faceheight = 103

[http://howtorecognise.mine.nu/feeds/categories/9-Planeta-Debian.rss]
name = Martín Ferrari
face = tincho.png
facewidth = 100
faceheight = 100

# 2012-06-09 - joerg - 404
# [http://lavaramano.livejournal.com/data/rss?tag=debian-es]
# name = Mauro Lizaur 
# face = mauro.png
# facewidth = 79
# faceheight = 100

[http://www.miriamruiz.es/weblog/?cat=6&feed=rss2]
name = Miriam Ruiz
face = miry.png
faceheight = 95
facewidth = 95

[http://criptonita.com/~nacho/blog/category/spanish/feed/]
name = Nacho Barrientos Arias

[http://elchipote.wordpress.com/category/planetadebian/feed/atom/]
name = Norman García
face = n0rman.png 


[http://churropolis.org/?feed=rss2&cat=9]
name = René Mayorga
face = rmayorga.png
facewidth = 90
faceheight = 102

[http://www.nul-unu.com/blogs/elucubrando/categorias/software-libre/debian/feed]
name = Rodrigo Gallardo

[http://blog.manty.net/feeds/posts/default/-/DebianEs]                        
name = Santiago García Mantiñán
face = manty.png
facewidth = 77
faceheight = 90

[http://billy.com.mx/category/debian/feed/]
name = William Vera 
face = william.png
facewidth = 85
faceheight = 95
